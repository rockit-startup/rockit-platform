package com.platform.rockit.controller;

import com.platform.rockit.model.WorkoutModel;
import com.platform.rockit.service.WorkoutService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.util.List;

@RestController
@RequestMapping("/api/v1/workout")
public class WorkoutController {
    private static final Logger LOGGER = LoggerFactory.getLogger(WorkoutController.class);

    @Autowired
    private WorkoutService workoutService;

    @GetMapping
        public ResponseEntity<Flux<WorkoutModel>> workoutSearch(){
        Flux<WorkoutModel> workoutModels = workoutService.workoutSearch();
        LOGGER.info("getAllNewsPaged, news ok.");
        return ResponseEntity.ok(workoutModels);
    }

}
