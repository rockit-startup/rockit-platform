package com.platform.rockit.external.facade;

import com.platform.rockit.external.facade.spotify.SpotifyExternalDataWorker;
import com.platform.rockit.external.facade.youtube.YoutubeExtenalDataWorker;
import org.springframework.web.client.RestTemplate;

import java.util.Collection;
import java.util.List;

public class ApiExternalDataFacade {

    RestTemplate restTemplate;

    private final List<ApiExternalDataProvider> providers;

    public ApiExternalDataFacade(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        providers = List.of(
                new YoutubeExtenalDataWorker(),
                new SpotifyExternalDataWorker());
    }

    public void seachExternalData() {
        makeActions(providers, ApiExternalDataProvider.Action.WORK);
    }

    private static void makeActions(Collection<ApiExternalDataProvider> providers,
                                    ApiExternalDataProvider.Action... actions) {
        providers.forEach(provider -> provider.action(actions));
    }
}
