package com.platform.rockit.external.facade;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public abstract class ApiExternalDataProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApiExternalDataProvider.class);

    public void goToRun() {
        LOGGER.info("{} goes to sleep.", name());
    }

    private void action(Action action) {
        switch (action) {
            case GO_TO_RUN:
                goToRun();
                break;
            case WORK:
                work();
                break;
            default:
                LOGGER.info("Undefined action");
                break;
        }
    }

    public void action(Action... actions) {
        Arrays.stream(actions).forEach(this::action);
    }

    public abstract void work();

    public abstract String name();

    enum Action {
        GO_TO_RUN, WORK
    }
}
