package com.platform.rockit.external.facade.spotify;

import com.platform.rockit.external.facade.ApiExternalDataProvider;
import com.platform.rockit.external.facade.youtube.YoutubeExtenalDataWorker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SpotifyExternalDataWorker extends ApiExternalDataProvider {
    private static final Logger LOGGER = LoggerFactory.getLogger(YoutubeExtenalDataWorker.class);

    @Override
    public void work() {
        LOGGER.info("{} creates another promising tunnel.", name());
    }

    @Override
    public String name() {
        return "Spotify External Data Worker name";
    }
}
