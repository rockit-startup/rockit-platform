package com.platform.rockit.external.facade.youtube;

import com.platform.rockit.external.facade.ApiExternalDataProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class YoutubeExtenalDataWorker extends ApiExternalDataProvider {
    private static final Logger LOGGER = LoggerFactory.getLogger(YoutubeExtenalDataWorker.class);

    @Override
    public void work() {
        LOGGER.info("{} creates another promising tunnel.", name());
    }

    @Override
    public String name() {
        return "Youtube External Data Worker name";
    }
}
