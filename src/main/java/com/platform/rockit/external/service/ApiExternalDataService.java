package com.platform.rockit.external.service;

import com.platform.rockit.model.WorkoutModel;
import reactor.core.publisher.Flux;

public interface ApiExternalDataService {
    void workoutExternalData();
}
