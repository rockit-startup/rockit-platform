package com.platform.rockit.external.service.imp;

import com.platform.rockit.external.facade.ApiExternalDataFacade;
import com.platform.rockit.external.service.ApiExternalDataService;
import com.platform.rockit.model.WorkoutModel;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import reactor.core.publisher.Flux;

@Service
public class ApiExternalDataServiceImp implements ApiExternalDataService {

    ApiExternalDataFacade apiExternalDataFacade;

    public ApiExternalDataServiceImp (RestTemplate restTemplate){
        this.apiExternalDataFacade = new ApiExternalDataFacade(restTemplate);
    }

    @Override
    public void workoutExternalData() {
        apiExternalDataFacade.seachExternalData();
    }
}
