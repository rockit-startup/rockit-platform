package com.platform.rockit.repository;

import com.platform.rockit.model.WorkoutModel;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface WorkoutRepository extends ReactiveCrudRepository<WorkoutModel, String>{
}
