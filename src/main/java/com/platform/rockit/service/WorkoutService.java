package com.platform.rockit.service;

import com.platform.rockit.model.WorkoutModel;
import reactor.core.publisher.Flux;

public interface WorkoutService {
    Flux<WorkoutModel> workoutSearch();
}
