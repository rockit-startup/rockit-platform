package com.platform.rockit.service.imp;

import com.platform.rockit.external.service.ApiExternalDataService;
import com.platform.rockit.model.WorkoutModel;
import com.platform.rockit.repository.WorkoutRepository;
import com.platform.rockit.service.WorkoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class WorkoutServiceImp implements WorkoutService {

    @Autowired
    WorkoutRepository workoutRepository;

    @Autowired
    ApiExternalDataService apiExternalDataService;

    @Override
    public Flux<WorkoutModel> workoutSearch() {

        Flux<WorkoutModel> workouts = workoutRepository.findAll();

        Mono<Long> countWorkouts = workouts.count();

        if(countWorkouts.block() <= 0l){
            //Get External Data Workout
            apiExternalDataService.workoutExternalData();
        } else{

        }


        return null;
    }
}
